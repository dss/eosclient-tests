#!/usr/bin/env python3

import os
import subprocess
import time
import tempfile
import socket
import argparse
import glob
import sys

test_def = {
  'all' : 'microbench_tests/* functional_tests/*',
  'all_excluded' : 'microbench_tests/*_fortran_* functional_tests/hardlink.sh functional_tests/fsping.sh functional_tests/createrepo.sh functional_tests/git-clone.sh functional_tests/inplace_edits.sh functional_tests/test_eos_rm_slow.sh microbench_tests/*_f77uf*',

  'functional' : 'functional_tests/*',
  'functional_excluded' : '',

  'microbench' : 'microbench_tests/*',
  'microbench_excluded' : '',

  'regression' : 'regression_tests/*',
  'regression_excluded' : '',

  'ci' : "microbench_tests/* functional_tests/test_sqlite.py functional_tests/rsync.sh functional_tests/test_eos_rm_missing*.sh functional_tests/test_eos_mv_corruption.sh",
  'ci_excluded' : "microbench_tests/*_fortran_* microbench_tests/*_f77uf*",

  # @note: like CI_eosfuse...
  'ci-eosfuse_release' : "functional_tests/test_sqlite.py functional_tests/rsync.sh functional_tests/zlib-compile-selftest.sh functional_tests/git-clone.sh functional_tests/funnychars.sh", 
  'ci-eosfuse_release_excluded' : "functional_tests/manyfile_open_nonread.pl functional_tests/createrepo.sh functional_tests/inplace_edits.sh",

  'windows' : "windows/0*.ps1 windows/0*.bat",
  'windows_excluded' : '',

  }

BINDIR=os.path.dirname(__file__)

parser = argparse.ArgumentParser()
parser.add_argument('--workdir', action='store', dest='workdir', default=BINDIR, help='eos fuse mount directory to run the tests (one or more directories separated by spaces)')
parser.add_argument('--monitoring', action='store_true', default=False, dest='monitoring', help='enable monitoring')
parser.add_argument('--silent', action='store_true', default=False, dest='silent', help='print errors only')
parser.add_argument('--instance-name',action='store',default="eos", dest='instance_name',help='instance name for reporting')
parser.add_argument('--disable-exitcode', action='store_false', default=True, dest='report_exitcode', help='disable reporting non-zero exit codes in case of failed tests')
parser.add_argument('--file',action='store_true',default=False,dest='single_file_mode',help='test to run is specified by the filename')
parser.add_argument('test',action='store',default=None, help="set of tests to run: %s" % ",".join(sorted([x for x in test_def.keys() if "_excluded" not in x])))

args = parser.parse_args()

def expand_pattern(name):
  files=[]
  for pattern in test_def[name].split(" "):
    for t in glob.glob(os.path.join(BINDIR,pattern)):
      files.append(os.path.abspath(t))
  return files

if args.single_file_mode:
  tests = [args.test]
else:
  tests = expand_pattern(args.test)
  excluded_tests = expand_pattern(args.test+"_excluded")
  tests = sorted(list(set(tests)-set(excluded_tests)))

WORKDIRS=[os.path.abspath(tempfile.mkdtemp(dir=d)) for d in args.workdir.split()]

CARBON_HOSTS = ('filer-carbon.cern.ch',)
CARBON_PORT = 2003

def send_message(message, timestamp = None):
  if timestamp is None:
    timestamp = time.time()

#  print '%s %d' % (message, timestamp)
  for CARBON_HOST in CARBON_HOSTS:
    try:
      conn = socket.create_connection((CARBON_HOST, CARBON_PORT))
      conn.send(('%s %d\n' % (message, timestamp)).encode())
      conn.close()
    except (OSError, IOError):
      # ignore any connection errors to avoid spamming
      pass

for d in WORKDIRS:
  if not os.path.isdir(d):
    raise Exception('WORKDIR: %s does not exist' % d)

success = True

for bin in tests:
  test = os.path.basename(bin)
  if 'windows' not in args.test:
    cmd = ['timeout', '400', bin]
  elif 'ps1' in bin:
    # TODO use a timeout.ps1 script to mimick timeout
    cmd = ['C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe', bin]
  else:
    cmd = ['C:\\Windows\\System32\\cmd.exe', '/C', bin]
  cmd += WORKDIRS

  if not args.silent:
    print('running', cmd)

  start = time.time()
  try:
    output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    duration = time.time() - start
    status = 0
  except subprocess.CalledProcessError as e:
    # negative timing means test failures
    duration = start - time.time()
    status = e.returncode
    output = e.output
    success = False

  if not args.silent:
    message = 'eos.clients.fuse.%s.%s\n time: %.3fms\n status: %d\n command: %s\n output: %s\n' % (args.instance_name, test, duration * 1000, status, cmd, output)
    print(message)

  if args.monitoring:
    testname = test.split('_', 1)[1]
    message = 'eos.clients.fuse.%s.microtests.%s_ms %.3f' % (args.instance_name, testname.replace('.ps1', '').replace('.bat', ''), duration * 1000)
    send_message(message, int(start))

if success or not args.report_exitcode:
  sys.exit(0)
else:
  sys.exit(1)
