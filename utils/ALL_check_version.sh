#! /bin/bash -e

# W.I.P.
# Note that, since EOS-4062, runs on Centos8 needs manual
# `export KRB5CCNAME=FILE:/tmp/<whatever>; kinit <user>@CERN.CH -k -t /somepath/some.keytab; eosfusebind -g; ls /eos/home-<u>/`

# Regular Colors
Color_Off='\033[0m'       # Text Reset
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow


echo
DIRNAME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # full directory name of this script no matter where it is being called from
echo "DIRNAME=$DIRNAME"
RELEASE_TESTS="$(dirname $DIRNAME)/functional_tests/ALL_eosfuse_release_tests.sh"
echo "RELEASE_TESTS=$RELEASE_TESTS"
echo

echo "Q1. Are the installed rpms what we expect?"
echo -e "${Yellow}Running check_version_installed.sh${Color_Off}"
$DIRNAME/check_version_installed.sh
echo

echo "Q2. Is the currently running eosxd process from after the update?"
echo -e "${Yellow}Running check_version_deployed.sh${Color_Off}"
$DIRNAME/check_version_deployed.sh
echo

echo "Q3. Does the eos update pulled in any unexpected dependency?"
echo -e "${Yellow}Running check_version_dependencies.sh${Color_Off}"
$DIRNAME/check_version_dependencies.sh
echo

# Q4. # Launch the functional tests at last
# For automation, better to keep the per-test timeouts and re-tune them for 4.*
# Else the test might be stuck forever, or so outrageously slow that this ought be considered a "fail".
# export NO_TIMEOUT=1
# < prepare >
# < run > eosclient-tests/functional_tests/ALL_eosfuse_release_tests.sh /eos/user/<x>/<xuserx>/tmp/...
# < collect >
EOSUSER=$(klist -l | tail -n 1 | awk '{print $1}' | cut -d "@" -f 1) # current unix user is not necessarily the krb5-authenticated eos user
EOSHOME="/eos/user/${EOSUSER::1}/$EOSUSER"
eostestdir=${1:-$EOSHOME/eostestdir}
echo "Q4. Now the functional tests can be run."
echo -e "${Yellow}Running $RELEASE_TESTS $eostestdir ...${Color_Off}"
time $RELEASE_TESTS $eostestdir
