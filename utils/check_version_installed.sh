#! /bin/bash -e

# Q1
# Proper version installed, i.e. `rpm -qi eos-fusex-core` _versus_ $VERS_VALIDATING


function usage () { 
    echo "Usage: $0 [<eos version>]"
    echo "Check if 'eos-fusex-core' is updated to the latest version, or to [<eos version>] if an argument is passed"
}

# VERS_VALIDATING is either :
# - passed as first arg	  -> Test explicitly if $1 matches the currently installed version
# - empty 				  -> No arg and yum list fails since the last update HAS been installed
#						     No need to get the currently installed version    : return SUCCESS
# - populated by yum list -> Yhe last available version HAS NOT been installed : return FAIL
VERS_VALIDATING=${1:-$(yum list updates eos-fusex-core 2> /dev/null | grep eos-fusex-core | awk '{print $2}' | cut -d'-' -f1)}

if [[ -z "$VERS_VALIDATING" ]]; then
    echo "OK" && exit 0
fi
vers_installed=$(rpm -q --queryformat '%{VERSION}' eos-fusex-core)
if [[ "x$VERS_VALIDATING" != "x$vers_installed"  ]]; then
    echo "Wrong version installed! Version $vers_installed found, while expecting $VERS_VALIDATING" 
    usage && exit -1
else
    echo "OK" && exit 0
fi
