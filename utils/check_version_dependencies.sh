#! /bin/bash -e

# Q3 
# Proper dependencies installed/updated, i.e. `repoquery`. Can double check by reading /var/log/distro_sync.log or /var/log/dnf.rpm.log`

# The OSes obviously share the majority of deps, while some are OS specific.
# It would be nice to get an homogeneous whitelist of allowed deps, and/or 
# understand and state clearly where the differences come from. 
# For slc6: the effort is not worth, since is going to an end of support
# For cc7,c8: quite a similarity, mismatches mostly due to the presence in C8 OS of updates missing in CC7 (details below)


function usage () { 
    echo "Usage: $0"
    echo "Checks if the latest eos rpms installed depend on a predefined set of rpm packages, fails if not"
}


######### Start with two tools:

# The list of eos packages published on Koji (not all of them have to be considered actually) ...
common_eoskoji_rpms=( eos-client eos-fuse eos-fuse-core eos-fuse-sysv eos-fusex eos-fusex-core eos-fusex-selinux ) # @note 'eos-fusex-cern-autofs' not wanted
  slc6_eoskoji_rpms=( "${common_eoskoji_rpms[@]}" )
   cc7_eoskoji_rpms=( "${common_eoskoji_rpms[@]}" )
    c8_eoskoji_rpms=( "${common_eoskoji_rpms[@]}" )

# ...and the list of declared and accepted rpm dependecies, which is, for each OS:
eosplus611_deps=( attr bash bzip2-libs chkconfig eos-client eos-fuse-core eos-fusex eos-fusex-core eos-fusex-selinux fuse fuse-libs \
    glibc initscripts jemalloc jsoncpp krb5-libs libcurl libevent2 libgcc libstdc++ libuuid ncurses-libs openssl policycoreutils protobuf3 readline \
    squashfs-tools sysvinit-tools xrootd-client xrootd-client-libs xrootd-libs zeromq3 zlib )
eosplus711_deps=( attr bash bzip2-libs eos-client eos-fuse-core eos-fusex-core eos-fusex-selinux eos-protobuf3 eos-xrootd fuse fuse-libs \
    glibc jemalloc jsoncpp krb5-libs libcurl libevent libgcc librichacl libstdc++ libuuid ncurses-libs openssl-libs policycoreutils readline \
    richacl squashfs-tools systemd zeromq zlib )
eosplus811_deps=( attr bash bzip2-libs eos-client eos-fuse-core eos-fusex-core eos-fusex-selinux eos-xrootd fuse fuse-libs \
    glibc jemalloc jsoncpp krb5-libs libcurl libevent libgcc librichacl libstdc++ libuuid ncurses-libs openssl-libs policycoreutils protobuf readline \
    richacl squashfs-tools systemd zeromq zlib )


# Such predefined lists ought to be compared to the current eos version's resolved rpm dependencies list, obtained then with something like
# $(repoquery --resolve --requires --installed --qf='%{NAME}' ${common_eoskoji_rpms[@]} | sort -u | tr '\n' ' ')


#### Let's start!

if [[ "$(cat /etc/redhat-release)" =~ "Scientific Linux CERN SLC release 6" ]]; then
    expected_deps=${eosplus611_deps[@]}
elif [[ "$(cat /etc/redhat-release)" =~ "CentOS Linux release 7" ]]; then
    expected_deps=${eosplus711_deps[@]}
elif [[ "$(cat /etc/redhat-release)" =~ "CentOS Linux release 8" ]]; then
    expected_deps=${eosplus811_deps[@]}
else 
    echo 'Wrong Operating System detected!'
    echo 'Unfortunately the supported OSes are strictly Scientific Linux CERN SLC release 6*, CentOS Linux release 7* or CentOS Linux release 8*'
    exit -1
fi

current_deps=$(repoquery --resolve --requires --installed --qf='%{NAME}' ${common_eoskoji_rpms[@]} | sort -u | tr '\n' ' ') # note that the last element will have a trailing space

deps_diff=$(echo ${current_deps[@]} ${expected_deps[@]} | tr ' ' '\n' | sort | uniq -u) # note that if empty, it will contain the empty string (so element count would return 1, not 0)


if [[ ! "${deps_diff[@]}" == "" ]]; then
    echo -e "Some unexpected dependencies were found! \n${deps_diff[@]}"
    usage && exit -1
else
    echo "OK" && exit 0
fi
