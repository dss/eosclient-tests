#! /bin/bash -e

# Q2
# Proper version deployed, i.e. `rpm -qi eos-fusex-core` _versus_ (ps o ppid,pid,lstart,cmd -C eosxd)

function usage () { 
    echo "Usage: $0 "
    echo "Check if currently running 'eosxd' process is updated to the latest installed version"
}


version_deployed_since=0

# trigger automount and wait for an abitrary time to let it be ready (but let's keep it reasonable)
EOSUSER=$(klist -l | tail -n 1 | awk '{print $1}' | cut -d "@" -f 1) # current unix user is not necessarily the krb5-authenticated eos user
EOSHOME="/eos/user/${EOSUSER::1}/$EOSUSER"
ls $EOSHOME > /dev/null && sleep 1
# the mount should be there now, fail if not
if [[ ! $(ls "$EOSHOME") ]]; then
    echo "Mount not ready! Exiting with failure..."
    usage && exit -1
fi

if [[ $(ps -o lstart= --sort=-start_time -C eosxd | tail -n1) ]]; then
	version_deployed_since=$(date -d "$(ps -o lstart= -C eosxd | tail -n1)" +%s)
fi


version_installed_since=$(date -d "$(rpm -q --queryformat '%{INSTALLTIME:date}' eos-fusex-core)" +%s)


if [[ "$version_deployed_since" -le "$version_installed_since" ]]; then
    echo "Wrong version deployed! The currently running 'eosxd' process was started on $(date -d @$version_deployed_since)"
    echo -e "While latest version $(rpm -q --queryformat '%{VERSION}' eos-fusex-core) has been installed on $(date -d @$version_installed_since)" 
    usage && exit -1
else
    echo "OK" && exit 0
fi
