Collection point for various EOSFUSE related test cases.
========================================================

Tests are grouped in sets.

Run all tests: 

    ./run.py --workdir="/mnt/eos1 /mnt/eos2 ..." all
    
Run microbenchmarks as regular crons: 

    ./run.py --silent --monitoring --disable-exitcode microbench
    
Run CI tests: 

    ./run.py --workdir="/mnt/eos1 /mnt/eos2" ci

You can define arbitrary sets of tests to be run this way. Edit run.py script.

Test are simple executables (e.g. bash scripts) with one or more arguments which are (eos mounted) directories to test:

    my_tests/test001.sh /mnt/eos1 [/mnt/eos2 /mnt/eos3 ...]

Return 0 on success or non-zero on failure.

The *_tests directories which are collections of related tests. A set of tests (e.g. ci) may include tests from many directories.

Microtests
==========

These are focused on latency (and used to compare different storage systems). They should not take longer than 5min, else report an error.

These tests will run regularly on "eosplusmicro.cern.ch" (see list of cronjobs on that machine, puppet hostgroup: [eostest/bagplus/microtests](https://gitlab.cern.ch/ai/it-puppet-hostgroup-eostest/-/blob/qa/code/manifests/bagplus/microtests.pp)), and submit to a [Grafana dashboard](https://filer-carbon.cern.ch/grafana/d/000000050/eos-fuse-microtests?orgId=1). A subset of these tests run as well on "eossmbmicrotests.cern.ch" (a Windows Terminal Server box, no puppet) to probe DFS and the CERNBox Samba gateways, and submit to the same dashboard.


Run your tests in the Dockerized environment / How to develop new tests
=======================================================================

You should develop your new tests locally or on a new branch in this repository.
Once you have written your tests, you can try it with eos-docker.

Install docker 

    su
    yum install docker -y
    service docker start

The commands below must be run as root or a user in the docker group.

Install the dockerized EOS environment

    git clone https://gitlab.cern.ch/eos/eos-docker.git
    cd eos-docker
     
To test a tagged release (4.2.17 in this example):

    docker pull gitlab-registry.cern.ch/dss/eos:4.2.17
    
To test a commit release you need to pull a corresponding ci pipeline build (build 328926 in this example):

    docker pull gitlab-registry.cern.ch/dss/eos:328926

You can get the list of all pipeline ci builds here (number next to author's beautiful face/avatar): https://gitlab.cern.ch/dss/eos/pipelines

Start the system and give the name of the image that you pulled in the previous step (release build in this example) and check if all the containers are there:

    scripts/start_services.sh -i gitlab-registry.cern.ch/dss/eos:4.2.17
    docker ps -a


Prepare the test:

    docker exec -i eos-mgm-test eos chmod 2777 /eos/dockertest
    docker exec -i eos-mgm-test eos vid enable krb5
    docker exec -i eos-client-test mkdir /eos1/
    docker exec -di eos-client-test mount -t fuse eosxd /eos1/
    docker exec -i eos-client-test mkdir /eos2/
    docker exec -di eos-client-test mount -t fuse eosxd /eos2/

Clone the tests you want to run:

    docker exec -i eos-client-test git clone https://gitlab.cern.ch/dss/eosclient-tests.git

If you develop a new test you want to checkout from your dev branch:

    docker exec -i eos-client-test bash -c '(cd /eosclient-tests && git checkout <my_fancy_test_branch>)'

Now ready to run the tests.

Run the ci test suite:

    docker exec -i -u eos-user eos-client-test python /eosclient-tests/run.py --workdir="/eos1/dockertest /eos2/dockertest" ci
    
Run your new test individually, for example, `functional_tests/test_eos_rm_missing.sh`: (not ready yet)
    
    #docker exec -i -u eos-user eos-client-test python /eosclient-tests/run.py --workdir="/eos1/dockertest /eos2/dockertest" functional_tests/test_eos_rm_missing.sh
    

Tear down the system by running:

     scripts/shutdown_services.sh

If you find new issues, you can create a JIRA ticker with your findings, otherwise if everything is fine, you can merge your test to master.


Old notes 1
===========

These should be run as the _eostest_ user into an area writeable by that user (e.g /eos/user/e/eostest/tests/).
A correspoding keytab in installed on machines in the _eostest_ cluster, in /etc/krb5.keytab.eostest. To use, please run

    kdestroy; kinit -kt /etc/krb5.keytab.eostest eostest; eosfusebind

Old notes 2
===========

This area should hold functional tests.
API is to be defined, but any non-zero exit code is probably a bad sign.

The first argument is the EOS test top-level directory to use. Tests should create
a subdirectory underneath, and clean up afterwards (at least for successful tests).

Old notes 3
===========

This area is mean to hold regression tests, i.e.
tests for things that were once broken, and are believed
to be fixed in later releases.

Naming convention:
* please use the short name of the relevant JIRA ticket (EOS-123) as
  first part of the filename

