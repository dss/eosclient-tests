#!/bin/bash

# typically needs: export EOS_MGM_URL=root://eos-mgm1.eoscluster.cern.ch
echo $(date) $0 $*


cd /eosclient-tests && {
  false && {    # this is for tests only
    set -x
    [[ $(git status -sb) = *rtb_clone_test* ]] || {
        git checkout rtb_clone_test || exit $?
        cd -
        exec /eosclient-tests/clone_tests/clone_test.sh $*
    }
  }
}

set +x

echo EOS_MGM_URL $EOS_MGM_URL

U=${EOS_MGM_URL:=$2}        # use second parameter when there is no management URL, eos-backup needs one
[[ -z "$U" ]] && { echo Error, no valid EOS_MGM_URL specified; exit 1; }
Uhost=${U##root://}         # for the occasional xrdfs

j=-j        # -f = human readable output, -j json

sleep 12   # callback-problems with fusex otherwise, Andreas says wait for 1st heartbeat
            # symptom: after a clone /eos1/dockertest/proc/clone is still empty
rc=0
EOS=/eos1/dockertest
eos=/eos/dockertest
EOSB=backuptest

B=/tmp/Backup                 # backup media prefix
# alternative: backup in EOS (ideally cta)
eos -r 0 0 mkdir -p $eos/backupmedia
B=$U/$eos/backupmedia
Bm=$eos/backupmedia


fusex_settle_sleep() {
  # fusex may delay the last file close for 1s, which could cause the MGM to only see a single write, not write followed by update
  #set -x
  sleep 2         # wait for fusex to settle

}

# extract cloneId from backup file
extract_cloneId() {
  # set -x
  cloneId=0
  cPath=
  if [[ "$j" = -j ]]; then  # bloody json
      read jsonstr
      xx=${jsonstr//[{\}\"]}                        # t:123456789,c:12345678,T:blabla
      for i in ${xx//,/ }; do
        case $i in
            c:*)
                cloneId=${i:2:99}
                ;;
            p:*)
                cPath=${i:2:99}
                ;;
        esac
      done
      [[ -n "$cPath" ]] && printf -v clonePath "%d/%s" $cloneId $cPath
      : cloneId $cloneId clonePath $clonePath
  else
      read pathname cloneInfo < $1                  # first line only
      : path $pathname cloneInfo $cloneInfo         # log
      read stime cloneId <<<${cloneInfo//:/ }       # split by ':'
      : stime $stime cloneId $cloneId
  fi
}

eosls() {
    opts=()
    while [[ $1 = -* ]]; do
        opts+=($1)
        shift
    done
    t=${1##root://}
    h=${t%%//*}
    p=/${t##*//}

    xrdfs $h ls ${opts[*]} $p
}

clone_test() {
  # ...
  #set -x
  date

  ls /eos1
  ls $EOS
  ls $EOS/proc
  ls $EOS/proc/clone
  id
  klist -a
  eos whoami
  rm -rf $EOS/$EOSB/*
  sleep 1
  eos "ls -l $eos/$EOSB"

  sleep 2
  mkdir $EOS/$EOSB

  for i in {0..9}; do
      f=$(printf "f%02d" $i); date > $EOS/$EOSB/$f
  done

  fusex_settle_sleep

  ls -l $EOS/$EOSB
  eos "ls -l $eos/$EOSB"

  eos "oldfind $j -x sys.clone=? $eos/$EOSB"
  ls -l $EOS/proc/clone
  eos -r 0 0 fusex ls

date +%s.%N
  eos "oldfind $j -x sys.clone=+10 $eos/$EOSB" | tee /tmp/backupFile1
  eos ls -l $eos/proc/clone
date +%s.%N
  ls -ld $EOS/proc/clone
date +%s.%N
  ls -l $EOS/proc/clone
date +%s.%N

  extract_cloneId < /tmp/backupFile1

  [[ $(($cloneId)) == 0 ]] && exit 0            # supported only under quarkddb

  eos -r 0 0 attr ls $eos/proc/clone/$cloneId

  for i in {0..4}; do
      date +%s.%N
      f=$(printf "f%02d" $i); date >> $EOS/$EOSB/$f
      date +%s.%N
      sleep 1
  done

  fusex_settle_sleep

  eos find -f $eos/proc/clone
  eos find -f $eos/proc/clone/$cloneId | while read path; do eos ls -l $path;done
  
  ls -lR $EOS/proc/clone


  for i in {0..4}; do
      f=$(printf "f%02d" $i)
      extract_cloneId < <(grep $f /tmp/backupFile1)
      #read pathName cloneInfo < <(grep $f /tmp/backupFile1)
      #: pathName $pathName cloneInfo $cloneInfo
      #read stime clonePath<<<${cloneInfo//:/ }  # split by ':'
      #: stime $stime clonePath $clonePath

      eos ls -l $eos/proc/clone/$clonePath
      fClone=$EOS/proc/clone/$clonePath
      [[ -e $fClone ]] || {
          sleep 5
          ls -lR $EOS/proc/clone
          sleep 60
          ls -l $fClone
      }

      lc=($(wc -l $fClone))
      [[ ${lc[0]} != 1 ]] && exit 1
  done

  # check for clones
  eos "oldfind $j -x sys.clone==$cloneId $eos/$EOSB"

  # remove clone
  eos "oldfind $j -x sys.clone=-$cloneId $eos/$EOSB"       # different from above: '=-' / '=='

  #
  ### exercise the eos-backup clone, backup and restore functionality:
  #


  SUB1=$EOS/$EOSB/subdir1
  [[ -e $SUB1 ]] && rm -rf $SUB1
  mkdir $SUB1
  # make the hierarchy a little more interesting
  for i in {0..9}; do
      mkdir $SUB1/d$i
      for j in {0..9}; do
          cp /etc/passwd  $SUB1/d$i/p$j
          cp /etc/group  $SUB1/d$i/g$j
      done
  done

  tar -C $EOS/$EOSB -cf - . | { sleep 3; tar -C $SUB1 -xvf - --no-same-owner; }

  fusex_settle_sleep

  # complete clone and backup in one go
  cmd="eos-backup backup -U $U -B $B -S 10240 /eos/dockertest/backuptest"
  echo "$cmd"
  read xx cloneId1 xxx media1 xxxx<<<$($cmd|grep -v blob)
  : $xx $cloneId1 $xxx $media1 $xxxx
  echo cloneId1 $cloneId1 media $media1
  
  # now some changes for an incremental backup
  for i in {0..4}; do
    f=$(printf "f%02d" $i)
    echo update $EOS/$EOSB/$f; date >> $EOS/$EOSB/$f
    i=$((${RANDOM}%10))
    j=$((${RANDOM}%10))
    echo update $SUB1/d$i/p$j; date >> $SUB1/d$i/p$j
    echo update $SUB1/d$j/g$i; date >> $SUB1/d$j/g$i
  done
  fusex_settle_sleep

  # make a copy so we can verify the restored data
  [[ -e /tmp/Orig2 ]] && rm -rf /tmp/Orig2
  mkdir /tmp/Orig2
  tar -C $EOS/$EOSB -cf - . | tar -C /tmp/Orig2 -xf -
  ls -laR /tmp/Orig2 |head -10
  echo ...

  # an incremental clone based on first backup
  date +%s.%N
  cmd="eos-backup clone -U $U -B $B -P $cloneId1 /eos/dockertest/backuptest"
  echo "$cmd"
  read xx cloneId2 xxx cloneFile2 <<<$($cmd|grep -v blob)
  : rc=$? $xx $cloneId2 $xxx $cloneFile2
  echo cloneId2 $cloneId2 cloneFile $cloneFile2
  eos -r 0 0 attr ls $eos/proc/clone/$cloneId2

  # shake a little to show backup stands firm
  (
    for i in {0..9}; do
      sleep 0.3
      date +%s.%N
      f=$(printf "f%02d" $i)
      echo update $EOS/$EOSB/$f; date >> $EOS/$EOSB/$f
    done
    fusex_settle_sleep
  ) &

  # backup up those files
  cmd="eos-backup backup -U $U -B $B -F $cloneFile2 -S 10240 /eos/dockertest/backuptest"
  echo "$cmd"
  read xx cloneId2x xxx media2 <<<$($cmd|grep -v blob)
  : rc=$? $xx $cloneId2x $xxx $media2
  echo cloneId2x $cloneId2x media $media2

  wait # wait for the shaker


  xrdcp $media2/catalog -
  echo

  # restore the lot
  [[ -e /tmp/Restore2 ]] && rm -rf /tmp/Restore2
  cmd="eos-backup restore -U $U -F $media1/catalog,$media2/catalog /tmp/Restore2"
  echo $cmd
  $cmd

  # there should be no differences
  diff -r /tmp/Orig2 /tmp/Restore2 || { echo error, trees /tmp/Orig2 and /tmp/Restore2 are not identical; rc=1;}
  [[ $rc -eq 0 ]] && echo No difference

  # remove a few files, change a few files, back-up incrementally and restore again
  for m in {0..9}; do n=$((${RANDOM}%10)); date >> $SUB1/d$m/g$n; rm $SUB1/d$n/p$m; done
      

  # backup them up
  cmd="eos-backup backup -U $U -B $B -S 10240 -P $cloneId2 /eos/dockertest/backuptest"
  echo "$cmd"
  read xx cloneId3 xxx media3 <<<$($cmd|grep -v blob)
  : rc=$? $xx $cloneId3 $xxx $media3
  echo cloneId3 $cloneId3 media $media3

  # restore the lot and compare to live directory
  [[ -e /tmp/Restore3 ]] && rm -rf /tmp/Restore3
  cmd="eos-backup restore -U $U -F $media1/catalog,$media2/catalog,$media3/catalog /tmp/Restore3"
  echo $cmd
  $cmd

  # there should be no differences
  diff -r $EOS/$EOSB /tmp/Restore3 || { echo error, live tree not identical to /tmp/restore3; rc=1;}
  [[ $rc -eq 0 ]] && echo No difference


}

clone_test_2() {
    date
    addon1="scl-utils devtoolset-8 xfsprogs-devel devtoolset-8-binutils-devel bzip2-devel cppunit-devel cppzmq-devel"
    addon2="e2fsprogs-devel elfutils-devel eos-folly eos-protobuf3 eos-protobuf3-compiler eos-protobuf3-devel eos-rocksdb"
    addon3="fuse fuse-devel grpc grpc-devel grpc-plugins help2man hiredis-devel jemalloc-devel jsoncpp jsoncpp-devel"
    addon4="krb5-devel leveldb-devel libattr-devel libcurl-devel libevent libevent-devel libmicrohttpd libmicrohttpd-devel"
    addon5="librichacl librichacl-devel libuuid-devel ncurses-devel ncurses-static openldap-devel openssl openssl-devel openssl-static"
    addon6="readline-devel sparsehash-devel xrootd xrootd-client-devel xrootd-private-devel xrootd-server-devel zeromq zeromq-devel"
    addon7="zlib-devel zlib-static automake cyrus-sasl-devel devtoolset-8-dwz devtoolset-8-dyninst devtoolset-8-elfutils"

    yum clean all
    yum install --nogpg -y gcc-c++ cmake3 make rpm-build which git yum-plugin-priorities ccache epel-release rpm-sign $addon1 $addon2 $addon3 $addon4 $addon5 $addon6 $addon7

    S=$((1024*1024*10))
    mkdir $EOS/eosbuild
    cd $EOS/eosbuild
    git clone https://gitlab.cern.ch/dss/eos.git        # &
    #sleep 10

    cd eos
    date
    git checkout wip_bckp_clone5
    date
    git submodule update --init --recursive
    git submodule update --init --recursive
    git status

    sleep 1         # new backupid
    then=$(date +%s)
    read xx cloneB1 xxx mediaB1 <<<$(eos-backup backup -U "$U" -B $B -S $S $eos/eosbuild|grep -v blob)
    : rc=$? $xx $cloneB1 $xxx $mediaB1
    now=$(date +%s)
    echo backup $cloneB1 $(($now-$then))s
    #wait
    eosls -l $mediaB1

    mkdir build
    cd build

    scl enable devtoolset-8 -- cmake3 .. 
    rc=$?
    [[ $rc = 0 ]] || {
        cat CMakeFiles/CMakeOutput.log
        cat CMakeFiles/CMakeError.log
    }


    sleep 1         # new backupid
    then=$(date +%s)
    read xx cloneB2 xxx mediaB2 <<<$(eos-backup backup -U "$U" -B $B -S $S -P $cloneB1 $eos/eosbuild|grep -v blob)
    : rc=$? $xx $cloneB2 $xxx $mediaB2
    now=$(date +%s)
    echo backup $cloneB2 $(($now-$then))s

    eosls -l $mediaB2

    [[ $rc -eq 0 ]] && {
        scl enable devtoolset-8 -- make -j 2 EosFstIo EosCommonServer-Objects XrdEosFst-Objects
        rc=$?

        df -k
        eos -r 0 0 fs ls -l
        eos -r 0 0 fs ls --io

        sleep 1         # new backupid
        then=$(date +%s)
        read xx cloneB3 xxx mediaB3 <<<$(eos-backup backup -U "$U" -B $B -S $S -P $cloneB2 $eos/eosbuild|grep -v blob)
        : rc=$? $xx $cloneB3 $xxx $mediaB3
        now=$(date +%s)
        echo backup $cloneB3 $(($now-$then))s

        eosls -l $mediaB3

        # restore the lot
        rm -rf /tmp/Restore*
        then=$(date +%s)
        eos-backup restore -U "$U" -F $mediaB1/catalog,$mediaB2/catalog,$mediaB3/catalog /tmp/RestoreE
        now=$(date +%s)
        echo restore $cloneB1,$cloneB2,$cloneB3 $(($now-$then))s

        # there should be no differences
        diff -r $EOS/eosbuild /tmp/RestoreE
        rc=$?
        if [[ $rc -eq 0 ]]; then
            echo No difference
        else
            echo error, trees $EOS/eosbuild and /tmp/RestoreE are not identical
        fi

    }



}

[[ "$1" != prepare ]] && exit 0

for p in eos-archive python3-xrootd; do 
    rpm -q $p || { yum install -y eos-archive python3-xrootd; break; }
done


    set -x
    cd /eosclient-tests
    [[ -e clone_tests/eos-backup ]] && cp -p clone_tests/eos-backup /usr/sbin/eos-backup
    cd -
    set +x

    #export XRD_LOGLEVEL=Debug   ## Dump
    #export XRD_LOGFILE=/tmp/xrdlog.log

df -k
date
sleep 20


#eos -r 0 0 debug debug '*'
#eosxd set system.eos.debug debug /eos1
clone_test
    #cat /tmp/xrdlog.log
eos -r 0 0 debug info '*'
eosxd set system.eos.debug info /eos1
false && clone_test_2
echo rc=$rc
exit $rc
