

testdir="${1:-/eos/user/e/eos-user/tests}/bugfix-2229"
mkdir -p $testdir && cd $testdir

python3 - <<EOF

f = open("f1.dat",'w')
f.write("*"*100)
f.flush()
f.truncate()
f.close()

f = open("f2.dat",'w')
f.write("*"*100)
f.flush()
f.close()

EOF

good_md5="b44a4f28f174a0ba92a65a1ce9ce979d"

f1_md5=$(md5sum f1.dat | cut -c -32)
f2_md5=$(md5sum f2.dat | cut -c -32)

if [[ $f1_md5 != $good_md5 ]]; then
  echo ERROR: wrong content of truncated file: f1.dat >&2
  exit 1
fi

if [[ $f2_md5 != $good_md5 ]]; then
  echo ERROR: wrong content of regular file: f2.dat >&2
  exit 1
fi

echo OK >&2
exit 0
