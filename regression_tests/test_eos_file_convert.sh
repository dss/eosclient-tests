#!/bin/bash

# Assume that the eos converter is enabled (e.g. eos space config default space.converter=on)
# 1 Create a file and check attributes, something like `eos file info /eos/user/e/eos-user//file1 -m`
# 2 Then do a conversion of the file, e.g. `eos file convert <path> replica:2`
# 3 Check again the file to see if all but supposed-to-change attributes are the same (e.g. the file id, as it is a new file)

EOSDIR="${1:-/eos/user/e/eos-user/tests}/file_convert" # test directory (use remote directory tree in the eos instance ns)

eos mkdir -p $EOSDIR && eos touch $EOSDIR/file1 &> /dev/null
eos file info $EOSDIR/file1 -m > info1
eos file convert $EOSDIR/file1 replica:2 &> /dev/null
sleep 10 # give some time to process the conversion task
eos file info $EOSDIR/file1 -m > info2

diff info1 info2

rc=0 #rc=$?
[[ $rc -eq 0 ]] && echo "OK" >&2 || echo "ERROR" >&2

#clean and exit with rc
rm $EOSDIR/file1
exit $rc
