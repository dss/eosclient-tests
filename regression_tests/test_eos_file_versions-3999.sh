#!/bin/bash

# Given the fact that the folder already contains
# the necessary xattrs as seen on homes and project
# instances.For this test, we are interested in
# => sys.versioning="10"

EOSDIR="${1:-/eos/user/e/eos-user/tests}/file_versions" # test directory (use remote directory tree in the eos instance ns)
LOCALDIR=${2:-$EOSDIR} # fuse mount (local directory tree at the client mountpoint, usually matches EOSDIR)

fn=$(cat /proc/sys/kernel/random/uuid)
eos cp --silent /etc/passwd $EOS_MGM_URL/$EOSDIR/$fn
eos cp --silent /etc/passwd $EOS_MGM_URL/$EOSDIR/$fn
eos $EOS_MGM_URL file versions $EOSDIR/$fn > v1
mv $LOCALDIR/$fn $LOCALDIR/$fn-new
eos $EOS_MGM_URL file versions $EOSDIR/$fn-new > v2

diff v1 v2

rc=$?
[[ $rc -eq 0 ]] && echo "OK" >&2 || echo "ERROR" >&2

#clean and exit with rc
eos $EOS_MGM_URL rm ${EOSDIR}/${fn}-new >& /dev/null
exit $rc
