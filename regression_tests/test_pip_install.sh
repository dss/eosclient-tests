#!/bin/bash

# Assume that pip is installed
# Seen in SWAN in March 2017, worked on 4.1.14, stopped working in 4.1.18
# Applied workaround to use local fs via XDG_CACHE_HOME
#
# Loops forever on link("/eos/.../pip.1lO/pip/selfcheck.json.lock") = -1 ENOSYS (Function not implemented)
#
## Upstream bug: https://github.com/pypa/pip/pull/2796 - Fixed as of pip-7.0.0

# It is enough to have pip install area locally (PYTHONUSERBASE in /tmp) to reproduce this error. 
# However we point the install to eos to increase coverage of this test.
#export PYTHONUSERBASE=`mktemp -d /tmp/pip.install.XXX` 

testdir="${1:-/eos/user/e/eos-user/tests}"

mkdir -p $testdir
export XDG_CACHE_HOME=$(mktemp -d "$testdir/pip.cache.XXX")
export PYTHONUSERBASE=$(mktemp -d "$testdir/pip.install.XXX")

# it is required that effective user id be the owner of the XDG_CACHE_HOME directory
# otherwise caching is disabled and test is a false-positive
if [[ $(id -un) != $(stat -c '%U' $XDG_CACHE_HOME) ]]; then
	echo "ERROR: must run as eos-user user"
	exit 1
fi

if [[ -z `which pip3` ]]; then
   PIP=pip
else
   PIP=pip3
fi

timeout 15 $PIP install --user hello_pip

rc=$?
[[ $rc -eq 0 ]] && echo "OK" >&2 || echo "ERROR" >&2

#clean and exit with rc
rm -rf $PYTHONUSERBASE $XDG_CACHE_HOME
exit $rc
