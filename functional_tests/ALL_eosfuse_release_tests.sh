#!/bin/bash
## wrapper script for the steps done manually in
## https://s3-website.cern.ch/eosops-docs/guides/fuse/fuserelease.html#validate-the-test-builds
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e
EOSTEST="$1"
QUIET=""
[[ -z "${EOSTEST}" ]] && { echo 'ERROR: need test directory as first argument' >&2 ; exit 50; }
[[ "${EOSTEST}" != /eos/* ]] && { echo "Warn: test directory ${EOSTEST} is not under /eos" >&2 ; }

[[ "$2" == '-q' ]] && QUIET=1

# this script lives in the same place as the actual tests
TESTREPO_DIR=$(dirname "$0")

if [[ -n "$NO_TIMEOUT" ]]; then
    echo "Warn: Timeouts have been turned off" >&2
fi

if [[ -n "$NO_STOP" ]]; then
    echo "Warn: will NOT stop on first error" >&2
fi

EOS_ignored_err=''
for t in test_sqlite.py rsync.sh zlib-compile-selftest.sh git-clone.sh manyfile_open_nonread.pl inplace_edits.sh funnychars.sh xauth.sh; do
  [[ -z "$QUIET" ]] && echo "Running ${t}" >&2
  if ! "${TESTREPO_DIR}/${t}" "${EOSTEST}"; then
      if [[ -z "$NO_STOP" ]]; then
	  echo "ERROR: from ${t} ***" >&2
	  exit 1
      else
	  [[ -z "$QUIET" ]] && echo "Warn: *** ignoring ERROR from ${t} ***" >&2
	  EOS_ignored_err=1
      fi
  fi
done

if [[ -n "$EOS_ignored_err" ]]; then
    echo 'ERROR: some tests had errors' >&2
    exit 1
else
    [[ -z "$QUIET" ]] && echo 'OK: All tests finished sucessfully' >&2
fi
