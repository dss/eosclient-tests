#!/bin/bash
## download and recompile zlib (this runs the internal self-tests at the end)
## supposed to reproduce a small project compilation on EOS
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e

testdir=${1:-/eos/user/e/eostest/tests}/zlib
mkdir -p "${testdir}"
rpmtmp=$(mktemp -d "${testdir}/${HOSTNAME}_zlib_XXXX")
tmptmp="${rpmtmp}/tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

# quite noisy, keep in logfile
rpmbuild --rebuild --define "_topdir ${rpmtmp}" --define "debug_package %{nil}" http://linuxsoft.cern.ch/cern/alma/9/BaseOS/source/Packages/zlib-1.2.11-40.el9.src.rpm 2>&1

rm -rf "${rpmtmp}"
echo "zlib: all OK"
exit 0
