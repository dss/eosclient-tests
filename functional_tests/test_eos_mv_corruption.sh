#!/usr/bin/env bash
TEST_PATH=$1
mkdir $TEST_PATH/eos_mv_bug
cd $TEST_PATH/eos_mv_bug
mkdir -p dir1/dir2
cp /etc/passwd dir1/dir2/f1.txt
mkdir dir2
cp /etc/passwd dir2/f2.txt
# Moving directory dir1/dir2 into the CWD directory should fail
mv dir1/dir2 . 2>&1 | grep "cannot move"

if [[ $? -ne 0 ]]; then
  echo "error: mv should fail if destination directory is not empty"
  exit 1
fi

rm -rf dir2/*
mv dir1/dir2 .

if [[ $? -ne 0 ]]; then
  echo "error: mv should succeed if destination directory is empty"
  exit 1
fi

cd ..
rm -rf eos_mv_bug
exit 0
