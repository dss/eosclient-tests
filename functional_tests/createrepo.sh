#!/bin/bash
## try to createrepo from local into EOS test area
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e
rpmdir=/eos/project/l/linux/cern/slc6X/x86_64/Packages/
testdir=${1:-/eos/user/e/eostest/tests}/createrepo
mkdir -p "${testdir}"
createrepotmp=$(mktemp -d "${testdir}/${HOSTNAME}_rsync_XXXX")
tmptmp="${createrepotmp}_tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

time createrepo --workers 4 --outputdir "${createrepotmp}" --verbose "${rpmdir}"

rm -rf "${createrepotmp}" "${tmptmp}"

echo "createrepo: all OK" >&2

exit 0
