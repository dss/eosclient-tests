#!/usr/bin/perl -w
# test case for ROOT opening many files, but not reading from them.
use File::Temp;
use File::Path;
use strict;

my $testdir='/eos/user/e/eostest/tests';
# normal 'ulimit' for files is 1024, but need some for current dir and the script
# cannot increase from within perl.
my $numfiles = `sh -c 'ulimit -n'` - 4;
my $totaldata = 1* 1024 * 1024 * 1024;

if($ARGV[0]) {
    $testdir = $ARGV[0]
}

File::Path::mkpath($testdir);
my $tempdir = File::Temp::mkdtemp($testdir.'/manyfile_open_nonread_XXXXXX') or die "PREP:mkdtemp failed:$!";

# prepare our files. Each will be bigger than previous
my $blocksize = int($totaldata * 2 / ($numfiles) / ($numfiles -1));
my $buf = 'a'x$blocksize;

for my $i (0..$numfiles) {
    my $tempfile =  $tempdir.'/file_'.$i;
    open (FH, '>', $tempfile ) or die "PREP: open($tempfile), i=$i/$numfiles, for write failed:$!";
    print FH  $i."\n" or die "PREP: write filenumber i=$i/$numfiles into ($tempfile) failed:$!";
    for my $j (0..$i) {
	print FH $buf or die "PREP: write block $j into ($tempfile), i=$i/$numfiles, failed:$!";
    }
    close(FH) or die "PREP: close($tempfile) failed:$!";
}

# now open all these files again for reading
my %fhs;
for my $i (0..$numfiles) {
    my $tempfile =  $tempdir.'/file_'.$i;
    open ($fhs{$i}, '<', $tempfile) or die "TEST: open($tempfile), i=$i/$numfiles for read failed:$!";
}
# and read from the last
my $line = readline $fhs{$numfiles};
chomp($line);
die "TEST: unexpected content: got '$line', expected '$numfiles'" unless ($line == $numfiles);

# cleanup - need to close files, else will run out of FDs in recursive remove
for my $i (0..$numfiles) {
    close $fhs{$i} or die "CLEANUP: cannot close filehandle $i/$numfiles:$!";
}
File::Path::remove_tree($tempdir) or die "CLEANUP: error on removing '$tempdir':$!";
   
1;
