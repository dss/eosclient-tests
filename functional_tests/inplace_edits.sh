#!/bin/bash
## play with in-place editing
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e

testdir=${1:-/eos/user/e/eostest/tests}/inplace_edit

mkdir -p "${testdir}"
tmpdir=$(mktemp -d "${testdir}/${HOSTNAME}_inplace_XXXX")
testfile="${tmpdir}/testfile"
tmptmp="${testdir}/tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"


echo "start" > "${testfile}"

# redirection order respected?
{ rm "${testfile}" && echo echo > "${testfile}"; } < "${testfile}"

# sed inline
sed -i -e s/echo/sed/ "${testfile}"

# perl inline
perl -p -i -e s/sed/perl/ "${testfile}"

# (convoluted) python inline
python3 -c $'import sys,fileinput\nfor l in fileinput.input(sys.argv[1],inplace=True):\n if "perl" in l:\n  print("python3")'  "${testfile}"

# VIM
vim -i NONE -c ':s/python3/vim' -c ':wq' "${testfile}"

# EMACS
emacs --quick --batch "${testfile}" --eval='(replace-regexp "vim" "emacs")' -f save-buffer

# NANO
expect -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../inputs/nano_edit.exp "${testfile}" >/dev/null

# final check - should be unhappy if something failed
grep -q nano "${testfile}" || { echo "in-place edits: something failed. File content:" >&2; cat "${testfile}" >&2;  exit 2 ; }

rm -rf "${testdir}"
echo "in-place edits: all OK" >&2
exit 0
