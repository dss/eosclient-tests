#!/bin/bash
## EOS-1811: RFE: support for "hard links" in FUSE
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e

# timeout for the whole test
if [[ -z "$NO_TIMEOUT" ]]; then
    testpid=$$
    ( sleep 10 && kill -SIGALRM "${testpid}" ) &
    sleeppid=$!
fi

testdir=${1:-/eos/user/e/eostest/tests}/hardlink
mkdir -p "${testdir}"
linktmp=$(mktemp -d "${testdir}/${HOSTNAME}_hardlink_XXXX")

touch "${linktmp}/source"
/bin/ln "${linktmp}/source"  "${linktmp}/target"

rm -rf "${linktmp}"
echo "hardlink: all OK" >&2

if [[ -z "$NO_TIMEOUT" ]]; then
    kill "${sleeppid}" >&/dev/null ||:
fi

exit 0
