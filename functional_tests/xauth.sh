#!/bin/bash
## 'xauth' does weird stuff with hardlinks,
##   gets triggered e.g via 'ssh -X'
## https://its.cern.ch/jira/browse/EOS-2894

trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e

testdir=${1:-/eos/user/e/eostest/tests}/xauth

mkdir -p "${testdir}"
tmpdir=$(mktemp -d "${testdir}/${HOSTNAME}_xauth_XXXX")
testfile="${tmpdir}/testfile"
tmptmp="${testdir}/tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

export XAUTHORITY=/nosuchfile/XAUTHORITY
dummy_display='12.34.56.78:0.0'
dummy_cookie='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'

echo -n 'add:'
echo "add ${dummy_display} MIT-MAGIC-COOKIE-1 ${dummy_cookie}" |\
   xauth -f "${testfile}" -v -n -

echo -n 'nextract:'
xauth -f "${testfile}" -v -n nextract - "${dummy_display}" |\
   grep "${dummy_cookie}" || {
    echo "ERROR: cannot read back the correct 'cookie' from ${testfile}"
    exit 2
   }
echo -n 'remove:'
xauth -f "${testfile}" -v -n remove "${dummy_display}" |\
    grep '1 entries removed' || {
    echo "ERROR: cannot remove entry from ${testfile}"
    exit 2
   }
echo -n 'list:'
xauth -f "${testfile}" -v -n list  |\
    grep -v '^Using authority file'|\
    grep '.' && {
    echo "ERROR: ${testfile} still has some entry after 'remove'"
    exit 2
   }
echo
rm -rf "${testdir}"
echo "xauth: all OK" >&2
exit 0
