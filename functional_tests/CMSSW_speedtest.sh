#!/bin/bash
## setup a CMS environment and compile
## based on RQF1588841, commands by Malik Shahzad Muzaffar
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e

# timeout for the whole test
# AFS:330sec, LXPLUS:/tmp: 260sec
TIMEOUT=900
if [[ -z "$NO_TIMEOUT" ]]; then
    testpid=$$
    ( command sleep "${TIMEOUT}"  && kill -SIGALRM "${testpid}" ) &
    sleeppid=$!
fi

testdir=${1:-/eos/user/e/eostest/tests}/cmssw
mkdir -p "${testdir}"
tmptmp=$(mktemp -d "${testdir}/${HOSTNAME}_cmssw_XXXX")
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

# "git cms-addpkg" wants this
if ! git config  --global --get user.github >/dev/null; then
	echo "## using tmp GIT config" >&2
	export XDG_CONFIG_HOME="${tmptmp}"
	mkdir -p "${XDG_CONFIG_HOME}/git"
	touch "${XDG_CONFIG_HOME}/git/config"
	git config --global --add user.github nobody
fi

cd "${tmptmp}"
source /cvmfs/cms.cern.ch/cmsset_default.sh
starttime=$(date "+%s")
echo '## Start: running "scram -a"' >&2
time scram -a slc7_amd64_gcc820 project CMSSW_11_1_0_pre8

cd CMSSW_11_1_0_pre8
echo '## running "scram runtime"' >&2
time eval $(scram runtime -sh)
echo '## running git cms-addpkg' >&2
time git cms-addpkg FWCore/Framework

echo "## Starting initial=full build"
time scram build -j 8

echo "## Starting second=unchanged build"
time scram build -j 8

endtime=$(date "+%s")
rm -rf "${tmptmp}"
echo "## End. cmssw: all OK, took $(( endtime - starttime ))sec" >&2


if [[ -z "$NO_TIMEOUT" ]]; then
    kill -HUP "${sleeppid}" >&/dev/null ||:
fi

exit 0
