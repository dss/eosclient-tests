#!/bin/bash

# This script should mirror 'ALL_eosfuse_release_test.sh', but for some tests who are unpractical for Continuos Integration pipeline execution.
# Namely, we are excluding
# - manyfile_open_nonread.pl :
#     as it is heavy for the resources usually allocated to the virtual machines/runners in CI
# - createrepo.sh :
#     as it uses "/eos/project/l/linux/cern/slc6X/x86_64/Packages/" and mounting other namespaces, auth the user, etcetera on CI is pretentious 
# - inplace_edits.sh :
#     as there in an issue with the chain of interactive/tty/login/detached stdin/out terminals specific to CI tests that needs to be undestood first.
#     It actually works otherwise.

## wrapper script for the steps done manually in
## https://s3-website.cern.ch/eosops-docs/guides/fuse/fuserelease.html#validate-the-test-builds
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e
EOSTEST="$1"
[[ -z "${EOSTEST}" ]] && { echo 'ERROR: need test directory as first argument' >&2 ; exit 50; }
[[ "${EOSTEST}" != /eos/* ]] && { echo "Warn: test directory ${EOSTEST} is not under /eos" >&2 ; }

# this script lives in the same place as the actual tests
TESTREPO_DIR=$(dirname "$0")

if [[ -n "$NO_TIMEOUT" ]]; then
    echo "Warn: Timeouts have been turned off" >&2
fi

if [[ -n "$NO_STOP" ]]; then
    echo "Warn: will NOT stop on first error" >&2
fi

EOS_ignored_err=''
#for t in test_sqlite.py rsync.sh zlib-compile-selftest.sh git-clone.sh manyfile_open_nonread.pl createrepo.sh inplace_edits.sh funnychars.sh; do
for t in test_sqlite.py rsync.sh zlib-compile-selftest.sh git-clone.sh funnychars.sh; do
  echo "Running ${t}" >&2
  if ! "${TESTREPO_DIR}/${t}" "${EOSTEST}"; then
      if [[ -z "$NO_STOP" ]]; then
	  echo "ERROR: from ${t} ***" >&2
	  exit 1
      else
	  echo "Warn: *** ignoring ERROR from ${t} ***" >&2
	  EOS_ignored_err=1
      fi
  fi
done

if [[ $( lsb_release -sr ) != 6* ]]; then
  t=xauth.sh   # does not work on SLC6, due to SELinux - EOS-3855
  echo "Running ${t}" >&2
  "${TESTREPO_DIR}/${t}" "${EOSTEST}"
fi

if [[ -n "$EOS_ignored_err" ]]; then
    echo 'ERROR: some tests had errors' >&2
else
    echo 'OK: All tests finished sucessfully' >&2
fi
