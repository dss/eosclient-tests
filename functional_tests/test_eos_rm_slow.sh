#!/bin/bash
# remove file that is being written to and check if it is properly 
# gone after inode was released

A=$1

mkdir -p $A/X

python - <<EOF $A/X/10.txt 10 1 &
import sys,time

fn=sys.argv[1]
cnt=int(sys.argv[2])
dt=float(sys.argv[3])

f = file(fn,"w")

while cnt>0:
    print >>f, cnt, " "
    print cnt
    time.sleep(dt)
    cnt-=1

print 'done'

EOF

sleep 3

rm $A/X/10.txt

sleep 8

cat $A/X/10.txt



