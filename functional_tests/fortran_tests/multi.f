      program multi
c
c This program opens a number of files in the directory
c passed as argument. After having opened all files,
c kmax integers are written to each file (unformatted).
c At the end it cleans up the output directory
c The number of files is controlled by the parameter ndescr
c
      parameter (ndescr=100)
      parameter (kmax=2000)

      character(len=1024) :: path
      character(len=1024) :: filename

c filenaming: #1=file.0011; #2=file.0012... to avoid to use low-number LUNs which are in general reserved (as 5,6)
      mindescr=11    
      maxdescr=mindescr+ndescr

      nn = iargc()
      if (nn.NE.1) then
         print *,'Error: One parameter required (output directory path)'
         call exit(3)
      endif

      call getarg(1,path)
c      print *,'Path',path

c      call remove_all(mindescr,maxdescr,path)

      call open_all(mindescr,maxdescr,path)

      do iunit=mindescr,maxdescr
         do k=1,kmax
            write(iunit) iunit+k
         enddo
      enddo

      call close_all(mindescr,maxdescr)

c      call remove_all(mindescr,maxdescr,path)

      end

      subroutine close_all(mindescr,maxdescr)
      do iunit=mindescr,maxdescr
         close(iunit)
      enddo
      end

      subroutine remove_all(mindescr,maxdescr,dir)
      character(*) dir
      CHARACTER(len=1024) :: filename
      CHARACTER(len=1024) :: fn

      do iunit=mindescr,maxdescr
         fn = filename(dir,iunit)
         open(iunit,file=fn,form='unformatted')
         close(iunit,status='delete')
      enddo
      end

      subroutine open_all(mindescr,maxdescr,dir)
      character(*) dir
      character(len=1024) :: filename
      character(len=1024) :: fn

      do iunit=mindescr,maxdescr
         fn = filename(dir,iunit)
         open(iunit,file=fn,form='unformatted',access='append')
      enddo
      end


      character(*) function filename(dir,i)
      character(*) :: dir
      write (filename,'(A,I0.4)') '/file.',i
      filename = trim(dir) // trim(filename)
      end 
