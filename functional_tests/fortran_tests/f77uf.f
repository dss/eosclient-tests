      program test

      character(len=2048) fn
      character(len=1024) path
      parameter (imax=1000,kmax=20)
      integer j(imax)
      integer lenght

      nn = iargc()
      if (nn.NE.1) then
         print *,'Error: One parameter required (output directory path)'
         call exit(3)
      endif

      call getarg(1,path)
      iii = length(path)

      write (fn,'(A,A)') path(1:iii),'/file.1'

      do i=1,imax
         j(i)=i
      enddo

c      open (3,file=fn,form="unformatted")
c      close(3,status="delete")

c      write(*,*) 'Filename',fn
c      write(*,*)' Appending',kmax,' blocks of',imax,' integers each'
c      write(*,1000) 'Writing block:'
      do k=1,kmax
         open (3,file=fn,form="unformatted",access="append")
c         write(*,1001) k 
         write(3) j
         close(3)
      enddo
c      write(*,*)

c      open (33,file=fn,status="replace")
c      close(33,status="delete")

1000  format(A,$)
1001  format(1x,I2,$)
      end

C FROM STACKOVERFLOW
      INTEGER FUNCTION LENGTH(STRING) !Returns length of string ignoring trailing blanks 
      CHARACTER*(*) STRING 
      DO 15, I = LEN(STRING), 1, -1 
        IF(STRING(I:I) .NE. ' ') GO TO 20 
 15       CONTINUE 
 20         LENGTH = I 
      END 
