#!/bin/bash
## simple 'git clone' (which may expose race conditions)
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e
echo "git-clone: starting" >&2

# timeout for the whole test
if [[ -z "$NO_TIMEOUT" ]]; then
    testpid=$$
    ( sleep 60 && kill -SIGALRM "${testpid}" ) &
    sleeppid=$!
fi

testdir=${1:-/eos/user/e/eostest/tests}/git-clone
mkdir -p "${testdir}"|| exit 1
gittmp=$(mktemp -d "${testdir}/${HOSTNAME}_git-clone_XXXX")
tmptmp="${gittmp}_tmp"
mkdir -p "${tmptmp}"
export TMP="${tmptmp}"
export TMPDIR="${tmptmp}"

git clone "https://:@gitlab.cern.ch:8443/dss/eosclient-tests.git" "${gittmp}"
pushd "${gittmp}" >/dev/null
git pull
git fsck --strict
popd >/dev/null

rm -rf "${gittmp}" "${tmptmp}"
echo "git-clone: all OK" >&2

if [[ -z "$NO_TIMEOUT" ]]; then
    kill "${sleeppid}" >&/dev/null ||:
fi

exit 0
