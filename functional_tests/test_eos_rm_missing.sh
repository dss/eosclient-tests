#!/bin/bash
A=$1
B=$2

time for i in {1..100}; do echo "A1 $i" > $A/$i; done

if [ -n "$TEST_EOS_RM1_READ_OTHER_CLIENT" ]; then
ls -l $B | wc -l
fi

rm $A/*

NA=`ls -l $A | wc -l`
# there is always a propagation time between clients since 'rm' is by default asynchronous as the propagation to clients is, we need to give some time
sleep 2
NB=`ls -l $B | wc -l`

if [[ $NA != $NB ]]; then
echo ERROR: mismatching number of directory entries A=$NA B=$NB
exit 1
else
echo OK
fi


