#!/bin/bash
#
# create filenames that have unexpected characters.
# list via 'find', compare checksum to known good value.

trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e


# shellcheck disable=SC1003,SC2016
function create_files {
    # simple stuff - whitespaces, line breaks, iso-latin-1 umlauts
    mkdir simple; cd simple
    declare -A shellchars

    shellchars=( \
	     ['whitespace']=' ' \
	     ['isoumlauta']=$'\xc4' \
             ['alert']=$'\a' \
             ['backspace']=$'\b' \
             ['escape']=$'\E' \
             ['formfeed']=$'\f' \
             ['newline']=$'\n' \
             ['carriagereturn']=$'\r' \
             ['horizontaltab']=$'\t' \
             ['verticaltab']=$'\v' \
             ['backslash']=$'\\' \
             ['singlequote']="'" \
             ['doublequote']='"' \
             ['questionmark']='?' )

    for c in "${!shellchars[@]}"; do
	d="${shellchars[$c]}"
	touch -- "${d}_initial_${c}_file"
	touch -- "middle_${d}_${c}_file"
	touch -- "final_${c}_file_${d}"
	mkdir -- "${d}_initial_${c}_dir"
	mkdir -- "middle_${d}_${c}_dir"
	mkdir -- "final_${c}_dir_${d}"
	ln -s -- "middle_${d}_${c}_file" "middle_${d}_${c}_link"
    done
    cd ..

    ## single char - overlap with above, but filenames above are nicer..
    mkdir singlecharloop; cd singlecharloop
    for c in {0..3}{0..7}{0..7}; do
	[[ "$c" == "000" || "$c" == "057" ]] && continue  # NULL char and / are not allowed
	d=$( printf '%b' "\\${c}" )
	touch -- "${d}_initial_${c}_file"
	touch -- "middle_${d}_${c}_file"
	touch -- "final_${c}_file_${d}"
	mkdir -- "${d}_initial_${c}_dir"
	mkdir -- "middle_${d}_${c}_dir"
	mkdir -- "final_${c}_dir_${d}"
	ln -s -- "middle_${d}_${c}_file" "middle_${d}_${c}_link"
    done
    cd ..

    ## some UTF8 = basic ASCII
    mkdir utf2charloop; cd utf2charloop
    for c in {1..255}; do
	[[ "$c" == "47" ]] && continue  # / is not allowed
	h=$( printf '00%X' "$c")
	d=$( printf '%b' "\\u${h}"  )
	touch -- "middle_${d}_${c}_file"
	ln -s -- "middle_${d}_${c}_file" "middle_${d}_${c}_link"
    done
    cd ..

    ## some random UTF8 - emoticons, page 1F6
    mkdir utfemoticons; cd utfemoticons
    for c in {0..255}; do
	h=$( printf '1F6%X' "$c" )
	d=$( printf '%b' "\\U${h}"  )
	touch -- "middle_${d}_${c}_file"
	ln -s -- "middle_${d}_${c}_file" "middle_${d}_${c}_link"
    done
    cd ..

    # stuff with side effects in a shell/command line
    mkdir shellbad; cd shellbad
    touch -- 'file with $VARIABLE'
    touch -- 'file with ${VARIABLE}'
    touch -- 'file with "${VARIABLE}"'
    touch -- 'file with $(COMMAND)'
    touch -- 'file with `COMMAND`'
    touch -- 'file with 2 \\'
    touch -- "file with ';COMMAND"
    touch -- 'file with ";COMMAND'
    touch -- 'file with |COMMAND'
    touch -- 'file with #COMMENT'
    cd ..

    # selected oddities for filesystems and shells
    mkdir fschar ; cd fschar
    touch -- '.'   # this dir
    touch -- './'  # also this dir
    touch -- '..'  # parent dir
    touch -- '...'
    touch -- ' .'
    touch -- '. '
    touch -- '\'
    touch -- '?'
    touch -- '*'
    touch -- ' '
    touch -- $'\u0001'  # backspace
    touch -- ';'
    touch -- '|'
    touch -- "'"
    touch -- '"'
    touch -- '&'
    touch -- '%'
    touch -- 'char[a-z]range'
    touch -- 'number{1..9}pattern'
    touch -- 'regexx*p.tt\.ern'
    cd ..

    # something that looks like EOS created it?
    mkdir eosspecials; cd eosspecials
    touch -- 'file'
    mkdir -- '.sys.v#.file'
    echo 'content1' > 'file'
    sync file 2>/dev/null # old 'sync' does not take arguments
    echo 'content2' > 'file'
    sync file 2>/dev/null # old 'sync' does not take arguments
    echo 'content3' > 'file'
    sync file 2>/dev/null # old 'sync' does not take arguments
    rmdir -- '.sys.v#.file'
    touch '.sys.a#.v#file.a383c75a-b801-11e7-886f-a0369f1fbf6c' ||\
	echo 'WARN: ignored' >&2
    cd ..

    # QuarkDB uses some inline separators?
    mkdir qdb ; cd qdb
    touch 'filename|.#something'
    touch 'filename|.##something'
    touch 'filename|.#'
    touch 'filename|.##'
    touch '|.#something'
    touch '|.##something'
    touch '|something|'
    cd ..

    # fair is fair - AFS has some special names..
    mkdir afsspecials ; cd afsspecials
    touch file
    sleep 1 > file &
    sync file  2>/dev/null # old 'sync' does not take arguments
    rm file
    rm -f .__afs* || echo 'WARN: ignored' >&2  # fails on AFS, EBUSY
    cd ..

    # HTML/URLs: all covered above
    ##  RFC1738 unsafe characters, reserved characters
    mkdir html ; cd html
    touch -- '&amp;'  '&#x26;'
    touch -- '&quot;' '&#x22;'
    touch --          '&#x2F;'  # /
    touch -- '&lt;'   '&#x3C;'
    touch -- '&gt;'   '&#x3E;'
    touch --          '&#x3F;'  # ?
    touch -- '&nbsp;' '&#xA0;'
    touch -- 'http:&#x2F;&#x2F;eospps.cern.ch&#x2F;&#x2F;eos&#x2F;test'
    touch -- 'root:&#x2F;&#x2F;eospps.cern.ch&#x2F;&#x2F;eos&#x2F;test?eos.param=foo&eos.ruid=0'
    cd ..

    # encodings: nothing says that bytes have to be ASCII, or valid UTF8 (python likes to assume that)
    ## list from https://www.php.net/manual/en/reference.pcre.pattern.modifiers.php#54805
    mkdir utf8; cd utf8
    touch -- $'file with Valid 2 Octet Sequence \xc3\xb1'
    touch -- $'file with Invalid 2 Octet Sequence \xc3\x28'
    touch -- $'file with Invalid Sequence Identifier \xa0\xa1'
    touch -- $'file with Valid 3 Octet Sequence \xe2\x82\xa1'
    touch -- $'file with Invalid 3 Octet Sequence (in 2nd Octet) \xe2\x28\xa1'
    touch -- $'file with Invalid 3 Octet Sequence (in 3rd Octet) \xe2\x82\x28'
    touch -- $'file with Valid 4 Octet Sequence \xf0\x90\x8c\xbc'
    touch -- $'file with Invalid 4 Octet Sequence (in 2nd Octet) \xf0\x28\x8c\xbc'
    touch -- $'file with Invalid 4 Octet Sequence (in 3rd Octet) \xf0\x90\x28\xbc'
    touch -- $'file with Invalid 4 Octet Sequence (in 4th Octet) \xf0\x28\x8c\x28'
    touch -- $'file with Valid 5 Octet Sequence (but not Unicode!) \xf8\xa1\xa1\xa1\xa1'
    touch -- $'file with Valid 6 Octet Sequence (but not Unicode!) \xfc\xa1\xa1\xa1\xa1\xa1'

    touch -- $'\u2215 - division slash'

    for i in file*; do
	ln -s -- "${i}" "${i}_link"
    done
    cd ..

    ## now should exercise different workflows
    ## copy, rename, sync
    ## would need to use different tools ('eos cp', CERNBox sync),
    ## since the filesystem seems OK so far
}



export LANG="C"
olddir="${PWD}"

## create reference on local disk
localdir=$(mktemp -d "/tmp/funnychar_${HOSTNAME}_XXXXXX")
echo "INFO: local ref area is ${localdir}"
cd "${localdir}"
create_files
cd "${olddir}"
echo "INFO: local ref area created"

## create reference on target area
testdir="${1:-/eos/user/e/eostest/tests}/funnychar"
mkdir -p "${testdir}"
tmpdir=$(mktemp -d "${testdir}/funnychar_${HOSTNAME}_XXXXXX")
echo "INFO: target area is ${tmpdir}"
cd "${tmpdir}"
create_files
cd "${olddir}"
echo "INFO: target area created"

#### compare ####
if ! diff -u -r \
       --exclude='.__afs*' \
       --exclude='.sys.a#.v#file*' \
       -- "${localdir}"  "${tmpdir}"; then
    echo "ERROR: there were differences" >&2
    exit 16
fi

# cleanup. If this fails, it still is an error :-)
if [[ -z "$NO_CLEAN" ]]; then
    rm -r "${localdir}" "${tmpdir}"
    # in particular if it did not actually clean up.. silently.
    if [[ -d "${tmpdir}" ]]; then
	echo "ERROR: cleanup on ${tmpdir} failed - still there" >&2
	exit 18
    fi
fi

echo "INFO: funnychar test is OK" >&2
