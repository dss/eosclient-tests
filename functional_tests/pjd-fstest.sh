#!/bin/bash
#
## wrapper for the TUXERA POSIX filesystem test suite
## default is to assume "ext3"
## *many* of these fail
#
## needs gcc, prove (perl-Test-Harness), libacl-devel

# exit on failure
# set -e

# timeout for the whole test
#sleep 600 && kill -SIGALRM "$$" &
#sleeppid=$!

# download and compile (local)
fsprobe=$(mktemp --tmpdir --directory pjd-fstest-XXXXX)
cd "${fsprobe}"
git clone https://git.code.sf.net/p/ntfs-3g/pjd-fstest ntfs-3g-pjd-fstest
cd ntfs-3g-pjd-fstest
make

# prepare test area
testdir=${1:-/eos/user/e/eostest/tests}/fsprobe
mkdir -p "${testdir}"
eostmp=$(mktemp -d "${testdir}/${HOSTNAME}_fsprobe_XXXX")
cd "${eostmp}"
echo "# test area is ${eostmp}"

# run test
prove -r "${fsprobe}"


# cleanup
rm -rf "${fsprobe}"
rm -rf "${eostmp}"

echo "fsprobe: all OK"
#kill "${sleeppid}" >&/dev/null || :
exit 0