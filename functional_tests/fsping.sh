#!/bin/bash
## runs Dan's "fsping" test
# first arg:  1. test mountpoint
# second arg: 2. test mountpoint, or
#            EOS server - then needs powers to (user)mount
trap 'echo "Error in $0:${LINENO}" >&2' ERR
set -e

testdir=${1:-/eos/user/e/eostest/tests}/fsping

mkdir -p "${testdir}"|| exit 1
fspingtmp=$(mktemp -d "${testdir}/${HOSTNAME}_fsping_XXXX")

if [[ -n "$2" && -d "$2" ]]; then
  fsping_mount2="$2/fsping"
else
  EOS_MGM="${2:-eosuat.cern.ch:}"
  fsping_mount2=$(mktemp -d "/tmp/fsping_mount_XXXXX")
  eosxd -ofsname="user@${EOS_MGM}${fspingtmp}" "${fsping_mount2}"
  eosxdpid=$(pgrep -u "${USER}" eosxd | xargs)
fi

fsping_git=$(mktemp -t -d "fsping_git_XXXXX")
git clone https://github.com/dvanders/fsping.git ${fsping_git}
fsping="${fsping_git}/fsping"

# start "server"
cd "${fspingtmp}"
"${fsping}" --server &
fspingpid=$!

# start client - no "sleep", will get addedd to initial latency
cd "${fsping_mount2}"
"${fsping}" --size=$(( 512*1024 ))
cd /

# will this stop by itself?
# assume so.

# cleanup - might exit itself
kill -9 "${fspingpid}" 2>/dev/null ||:

if [[ -n "$eosxdpid" ]]; then
  sleep 1  # give fsping server time to exit
  kill "${eosxdpid}"
  sleep 5  # give eosxd time to clean up 
  rmdir "${fsping_mount2}"
fi

rm -rf "${fspingtmp}" "${fsping_git}"

echo "FSPING: all OK?" >&2

exit 0
