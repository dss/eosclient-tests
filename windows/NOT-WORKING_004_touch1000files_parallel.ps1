# touch 1000 files in parallel

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

$Job = 1..1000 | ForEach-Object -Parallel -ThrottleLimit 100 -AsJob { New-Item -Path $using:path -Name "asdf.$_" -ItemType File -Force }
$Job | Wait-Job | Receive-Job

# $duration = Measure-Command { $Job | Wait-Job | Receive-Job }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds