﻿# delete all of the created artifacts via robocopy

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

function Clean-TempData {
    param (
        $Path
    )

    $location = ""

    if (-not ([string]::IsNullOrEmpty($Path))) {
        if ($Path -is [array] -and $Path.Length -gt 0) {
            $location = $Path[0]
        } else {
            # it is a string
            $location = $Path
        }

        # Create empty directory as an input for robocopy
        Set-Location -Path $("$location\..")
        New-Item -ItemType "directory" empty > $null

        $options = @("/copyall","/MIR","/E","/NFL","/NDL","/NJH","/NJS","/nc","/ns","/np")
        # /NFL : No File List - don't log file names.
        # /NDL : No Directory List - don't log directory names.
        # /NJH : No Job Header.
        # /NJS : No Job Summary.
        # /NP  : No Progress - don't display percentage copied.
        # /NS  : No Size - don't log file sizes.
        # /NC  : No Class - don't log file classes.

        $Path | ForEach-Object {
            $cmdArgs = @("empty","$_",$options)
            robocopy.exe @cmdArgs > $null
            Remove-Item $_ -Recurse -Force -Confirm:$false
        }

        Remove-Item empty -Recurse -Force -Confirm:$false
    }
}

Clean-TempData $path
