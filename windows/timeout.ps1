# Wrapper script that calls independent tests with timeout
#
# Parameters:   -path   : location of the script to run
#
# Created:		22/01/2020
# Author: 		Apostolos Smyrnakis
# Reviews:      
#-------------------------------------------------------------------------------------


param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path,
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$scriptPath,
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$timeoutSec
)

# 10 minutes
# $timeoutSec = 600

$scriptToRun = "{0} -path {1}" -f $scriptPath, $path
$argList = "-Command `"$scriptToRun`""
$process = Start-Process -FilePath powershell.exe -ArgumentList $argList -PassThru

try {
    $process | Wait-Process -Timeout $timeoutSec -ErrorAction Stop
    # Write-Host 'Process finished successfully.'
}
catch {
    # Write-Warning -Message 'Process was killeed!'
    $process | Stop-Process -Force
}