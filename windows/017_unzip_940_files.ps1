# unzip given file
# How to hide progress bar:
# https://stackoverflow.com/questions/46247608/powershell-hide-progress-of-expand-archive-cmdlet?noredirect=1&lq=1

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

$ProgressPreference = 'SilentlyContinue'

Expand-Archive -LiteralPath 'C:\MICROTESTS\inputs\ceph-0.30.zip' -DestinationPath $path -Force | Out-Null
# $duration = Measure-Command { Expand-Archive -LiteralPath 'C:\MICROTESTS\inputs\ceph-0.30.zip' -DestinationPath $path -Force }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds