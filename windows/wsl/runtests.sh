#!/bin/sh
#
# wrapper to run all microbench tests across all Windows-related endpoints:
# DFS, SMB, SMB+EOS

# refresh all mounts as they get auto-unmounted
~/recreatemounts.sh

cd ~/MICROTESTS
echo `date`: === START === >> /home/cboxtest/tests.log
for i in dfs haeossmb smb winlocal; do
  /usr/bin/python ./run.py --silent --monitoring --workdir=/mnt/${i} --instance-name ${i} microbench
  rc=$?
  echo `date`: $i: rc = $rc >> /home/cboxtest/tests.log
done
for i in dfs haeossmb smb winlocal; do
  #echo $i >> /home/cboxtest/tests.log
  sudo rm -rf /mnt/${i}/tmp*
done
echo `date`: END - cleanup completed >> /home/cboxtest/tests.log

~/umountall.sh

