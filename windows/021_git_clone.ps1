param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

Set-Location C:\
git.exe clone "https://gitlab.cern.ch/dss/eosclient-tests.git" $path\git-clone

Remove-Item -Path $path\git-clone -Force -Recurse

# $duration = Measure-Command { tar.exe -xf 'C:\MICROTESTS\inputs\ceph-0.30.tar.gz' }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds
