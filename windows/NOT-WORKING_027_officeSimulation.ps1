# run MS Office simulation
# Crates 5 .docx files, copies, edits and then deletes them 

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

# Option 1
# $cmd = '.\OfficeUsersSimulation_C.exe'
# $params = "nof 5 crtfls usewd autodel wrkdir `"$path`""
# $prms = $params.Split(" ")
# & "$cmd" $prms

# Option 2
$path = $path.TrimEnd('\')
Start-Process -NoNewWindow -Wait -FilePath "windows\OfficeUsersSimulation_C.exe" -ArgumentList "nof 5 crtfls usewd wrkdir `"$path`""

# $duration = Measure-Command { 
#     # Option 1
#     # $cmd = '.\OfficeUsersSimulation_C.exe'
#     # $params = "nof 5 crtfls usewd autodel wrkdir `"$path`""
#     # $prms = $params.Split(" ")
#     # & "$cmd" $prms

#     # Option 2
#     Start-Process -NoNewWindow -Wait -FilePath ".\OfficeUsersSimulation_C.exe" -ArgumentList "nof 5 crtfls usewd autodel wrkdir `"$path`""
# }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds