# robocopy of a very large tree (2.6 GB, the source DVD for MS SQL)

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

$ProgressPreference = 'SilentlyContinue'

function RoboCopy-Directory {
    param(
        [String]$source,
        [String]$dest
    )

    # Sebastian's script params
    $options = @("/E","/NFL","/NDL","/NJH","/NJS","/nc","/ns","/np")

    # exclusions (used for prod transfers)
    #$xd = @("Thumbs.db","~*","*.tmp")
    $xd = @("DfsrPrivate","*.DEL201*","$RECYCLE.BIN","$RECYCLER","RECYCLER")

    $cmdArgs = @("$source","$dest",$options)
    robocopy.exe @cmdArgs /XD @xd
}

# only test these storages
if($path -like "*ceph*" -or $path -like "*nast*" -or $path -like "*dfs*") {
    # the copy takes place from a pre-filled folder
    RoboCopy-Directory -source 'C:\MICROTESTS\inputs\largerobocopy' -dest $path\largerobocopy
}
else {
    write-host "skipping $path"
}

#$duration = Measure-Command { ... }
#$roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

#Return $roundedMilliseconds
