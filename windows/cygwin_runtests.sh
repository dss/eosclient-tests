#!/bin/sh
#
# wrapper to run all microbench tests across all Windows-related endpoints:
# DFS, SMB, SMB+EOS
# This is currently running as a Windows "cron" on a CDA-provided Terminal Server
#
# Author: Giuseppe Lo Presti

export PATH=/usr/bin:$PATH
cd /home/lopresti/MICROTESTS
echo `date`: === START === >> /home/lopresti/tests.log
for i in eossmb dfs smb winlocal haeossmb; do
  /usr/bin/python2.7.exe ./run.py --silent --monitoring --workdir=/home/lopresti/targets/${i} --instance-name ${i} microbench
  echo `date`: $i: $? >> /home/lopresti/tests.log 
done
for i in winlocal dfs smb eossmb; do
  #echo $i >> /home/lopresti/tests.log
  rm -rf /home/lopresti/targets/${i}/tmp*
done
echo `date`: END - cleanup completed >> /home/lopresti/tests.log

