# robocopy of a large tree + verification

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

$ProgressPreference = 'SilentlyContinue'

function RoboCopy-Directory {
    param(
        [String]$source,
        [String]$dest
    )

    # options: this list is adapted from Sebastian's script for DFS migrations
    #$options = @("/MT:16","/R:0","/W:0","/ZB","/NP","/MIR","/TEE","/NFL","/NDL","/XJD","/XO")
    # this list comes from INC3230984
    $options = @("/MT","/s","/e")

    # exclusions (used for prod transfers)
    #$xd = @("Thumbs.db","~*","*.tmp")
    $xd = @("DfsrPrivate","*.DEL201*","$RECYCLE.BIN","$RECYCLER","RECYCLER")

    $cmdArgs = @("$source","$dest",$options)
    robocopy.exe @cmdArgs /XD @xd
}

# the copy takes place from a pre-filled folder
RoboCopy-Directory -source 'C:\MICROTESTS\inputs\robocopy' -dest $path\robocopy

#$duration = Measure-Command { ... }
#$roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

#Return $roundedMilliseconds
