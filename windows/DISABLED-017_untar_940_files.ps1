# untar given file

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

Set-Location $path
tar.exe -xf 'C:\MICROTESTS\inputs\ceph-0.30.tar.gz'
# $duration = Measure-Command { tar.exe -xf 'C:\MICROTESTS\inputs\ceph-0.30.tar.gz' }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds