#
# wrapper to run all microbench tests across all Windows-related endpoints:
# DFS, Windows, SMB, SMB+EOS
# This is currently running as a Windows "cron" on a CDA-provided Terminal Server
#
# Maintainers: Giuseppe Lo Presti, Sebastian Bukowiec
# --------------------------------------------------------------------------------


$instances = @()
$paths = @()

#$instances += "winlocal"
#$paths += "C:\Temp"

###############

$instances += "dfs"
$paths += "\\cern.ch\dfs\Projects\eossambabot\dfsmicrotests"

#$instances += "winshare"
#$paths += "\\cerndata46\p-eossamba$\winsharemicrotests"

#$instances += "smb"
#$paths += "\\cernbox-smb-qa\test\local\microtests"

###############

#$instances += "smbceph"
#$paths += "\\cernbox-smb-qa\test\cephflaxhdd01\ops\microtests"

$instances += "smbceph-doyle"
$paths += "\\cernbox-smb-qa\test\cephdoylehdd01\ops\microtests"

$instances += "smbceph-ssd"
$paths += "\\cernbox-smb-qa\test\cephvaultssda01\ops\microtests"

#$instances += "smbceph-pam"
#$paths += "\\cernbox-smb-qa\test\cephpamhdd01\ops\microtests"

#$instances += "winceph"
#$paths += "\\cerndatatmp01\ceph\microtests"

#$instances += "smbzfs-ceph"
#$paths += "\\cbox-samba-vm01\test\cephzfs\microtests"

###############

#$instances += "smb-netapp"
#$paths += "\\nast-smb1\plmarasprod\microtests"

#$instances += "smb-netapp-ssd"
#$paths += "\\nast-smb1\plmarasprod_flash\microtests"

###############

# For the following EOSwnc tests to work, one has to launch on a PowerShell:
# setx EOS_CA_CERT C:\Users\eossambabot\CERNCA.crt
# setx EOS_CL_CERT C:\Users\eossambabot\usercert.pem
# setx EOS_CL_KEY C:\Users\eossambabot\userkey.pem
# 'C:\Program Files (x86)\EOS-wnc\eos-drive.exe' --endpoint eospps.cern.ch:50052:8443 --dir  /eos/pps/opstest/microtests

# for debugging, the eos console can be started as:
# 'C:\Program Files (x86)\EOS-wnc\eos.exe' --endpoint eospps.cern.ch:50052:8443

#$instances += "wnceos-pps"
#$paths += "P:\eos\pps\opstest\microtests\wnc"

#$instances += "wnceos-pilot"
#$path += "I:\pilot\opstest\wnc"

###############

#$instances += "smbeos"
#$paths += "\\cernbox-smb\eos\user\e\eossambabot\microtests"

#$instances += "smbeos-ssd"
#$paths += "\\cernbox-smb\eos\user\e\eossambabot\microtests-ssd"

#$instances += "smbeos-ceph"
#$paths += "\\cernbox-smb\eos\user\d\dcboxtest\sambatests\microtests-cephfs"

$instances += "smbeos-proj"
$paths += "\\eosproject-smb\eos\project\c\cernbox\Testing\probes\microtests-samba"

#$instances += "smbeos-canary"
#$paths += "\\cernbox-smb-qa\test\homecanary\homecanary\opstest\micro-samba"

#$instances += "smbeos-homedev"

#$instances += "smbeos-canary-ceph"
#$paths += "\\cbox-smb-test\test\homecanary\homecanary\opstest\micro-sambaceph"

#$instances += "smbeosqa"
#$paths += "\\cernbox-smb-qa\eos\user\e\eossambabot\microtests"

#$instances += "smbeosqa-ceph"

#$instances += "smbeosqa-ssd"

#$instances += "smbeos-pps"
#$paths += "\\cbox-smb-test\test\eospps\microtests"

################



cd C:\MICROTESTS
"$(Get-Date): === START ===" | Out-File -Append tests.log -Encoding utf8

ForEach ($i in 0..($instances.length-1)) {
    $cmd = "python.exe run.py --monitoring --workdir {0} --instance-name {1} windows" -f $paths[$i], $instances[$i]
    #"$(Get-Date): executing {0}" -f $cmd | Out-File -Append tests.log -Encoding utf8
    #Start-Process -Wait -NoNewWindow $cmd
    If (Test-Path $paths[$i]) {
        Invoke-Expression $cmd
        "$(Get-Date): {0}, rc={1}" -f $instances[$i], $LastExitCode | Out-File -Append tests.log -Encoding utf8
    } Else {
        "$(Get-Date): {0} skipped as unreachable" -f $instances[$i] | Out-File -Append tests.log -Encoding utf8
    }
}

"$(Get-Date): === END ===" | Out-File -Append tests.log -Encoding utf8
