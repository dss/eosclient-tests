# rm 100 files

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

Remove-Item $path\*
# $duration = Measure-Command { Remove-Item $path\* }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds