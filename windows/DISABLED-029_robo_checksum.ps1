# checksum all files previously copied via robocopy
# assumes the test 028_robocopy has already been executed

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

$reference_hash = "293C4F89A69C824B9D3F73375065FF91"

$hashesfile = "C:\Windows\Temp\rbchashes{0}" -f $path.Replace('\', '_').Replace(':', '') -replace "tmp.*", ''

Get-ChildItem -Path $path\robocopy -Recurse -File |
    Get-FileHash -Algorithm MD5 |
    Sort-Object Path |
    #Select-Object Hash, @{l="Name";e={Split-Path $_.Path -leaf}}
    Select-Object Hash |
    Export-Csv -Path $hashesfile -NoTypeInformation

$global_hash = (Get-FileHash $hashesfile -Algorithm MD5).hash
if ($global_hash -ne $reference_hash) {
    $failedfile = "C:\Windows\Temp\failed_rbc_checksum_{0}" -f $path.Replace('\', '_').Replace(':', '') -replace "tmp.*", ''
    Write-Host "Global hash is $global_hash, not as expected"
    Get-FileHash -Algorithm MD5 |
    Sort-Object Path |
    Export-Csv -Path $failedfile -NoTypeInformation
    exit -1
}

