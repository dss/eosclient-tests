# ls 100 files

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

Get-ChildItem $path
# # $duration = Measure-Command { ls -Path $path }
# $duration = Measure-Command { Get-ChildItem $path }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds