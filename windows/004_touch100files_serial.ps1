# touch 1000 files in serial

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

1..100 | ForEach-Object { New-Item -Path $path -Name "asdf.$_" -ItemType File -Force }
# $duration = Measure-Command { 1..100 | ForEach-Object { New-Item -Path $path -Name "asdf.$_" -ItemType File -Force } }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds
