# move untared files
# assumes the test 017_untar_940_files has already been executed

param(
    [parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]$path
)

New-Item -Path "$path\ceph-0.30" -Name "subdir" -ItemType "directory" | Out-Null
Move-Item -Path "$path\ceph-0.30\src" -Destination "$path\ceph-0.30\subdir"

# $duration = Measure-Command {
#     New-Item -Path $path -Name "subdir" -ItemType "directory" | Out-Null
#     Move-Item -Path "$path\ceph-0.30\src" -Destination "$path\ceph-0.30\subdir"
# }
# $roundedMilliseconds = [math]::Round($duration.TotalMilliseconds)

# Return $roundedMilliseconds
